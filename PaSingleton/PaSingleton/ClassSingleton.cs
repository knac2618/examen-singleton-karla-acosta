﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaSingleton
{
    class ClassSingleton
    { 
        // Una vez realizado esto  guardamos la unica instancia que va a existir
        private static ClassSingleton instancia;

        //datos propios de dicha clase
        private string nombre;
        private int edad;
        private int telefono;

        //creamos el constructor que sera de forma privada 
        public ClassSingleton()
        {
            // datos de la clase
            nombre = "sin asignar";
            edad = 92;
            telefono = 0996413094;
        }

        //creamos el metodo estatico, que va a crear una sola instancia 
        public static ClassSingleton oBtenerIntancia()
        {
            //verificamos si no existe la instancia
            if (instancia == null)
            // si no existe la instancia se crea 
            {
                instancia = new ClassSingleton();


                Console.WriteLine("instancia creada por primera vez");
            }
            //regresamos instancia
            return instancia;
        }
        //aqui metodos propios de la clase
        public override string ToString()
        {
            return string.Format("la persona {0}, tiene edad {1}, y su  telefono es {2}", nombre, edad, telefono);

        }

        public void PonerDatos(string pNombre, int pEdad, int ptelefono)
        {
            nombre = pNombre;
            edad = pEdad;
            telefono = ptelefono;
        }
        //esto representa cualquier otro objeto
        public void ProcesoDeClase()
        {
            Console.WriteLine("{0} esta trabajando en algo", nombre);
            //return string.Format("la persona {0}, tiene edad {1}, telefono{2}", nombre, edad, telefono);

        }





    }
}

