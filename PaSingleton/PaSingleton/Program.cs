﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PaSingleton
{
    class Program
    {
        static void Main(string[] args)
        {
            // No podemos obener una instancia directamente
            //la intancia que se crea por primera vez

            ClassSingleton  uno= ClassSingleton.oBtenerIntancia();
            //hacemos algo con la instancia
            uno.PonerDatos("Carlos", 20, 099413094);
            uno.ProcesoDeClase();
            uno.ProcesoDeClase();
            Console.WriteLine(uno);
            Console.WriteLine("------------------");
            //obtenemos dicha  instancia
            ClassSingleton dos = ClassSingleton.oBtenerIntancia();
            //comprobamos que es la misma instancia 
            //Como es la misma instancia, tendrá el mismo estado que hemos ingresado en la parte de  arriba
            Console.WriteLine(dos);
            Console.ReadKey();
        }
    }
}
